function varargout = MINCutGUI(varargin)
% MINCUTGUI MATLAB code for MINCutGUI.fig
%      MINCUTGUI, by itself, creates a new MINCUTGUI or raises the existing
%      singleton*.
%
%      H = MINCUTGUI returns the handle to a new MINCUTGUI or the handle to
%      the existing singleton*.
%
%      MINCUTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MINCUTGUI.M with the given input arguments.
%
%      MINCUTGUI('Property','Value',...) creates a new MINCUTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MINCutGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MINCutGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MINCutGUI

% Last Modified by GUIDE v2.5 19-Mar-2013 12:31:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MINCutGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @MINCutGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MINCutGUI is made visible.
function MINCutGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MINCutGUI (see VARARGIN)

% Choose default command line output for MINCutGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MINCutGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MINCutGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in KDD_Orig_But.
function KDD_Orig_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Orig_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDScript;

% --- Executes on button press in KDD_min_but.
function KDD_min_but_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_min_but (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDMin;

% --- Executes on button press in ICDE_Orig_but.
function ICDE_Orig_but_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Orig_but (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEScript;

% --- Executes on button press in ICDE_Min_But.
function ICDE_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEMin;

% --- Executes on button press in VLDB_Orig_But.
function VLDB_Orig_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Orig_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBScript;

% --- Executes on button press in VLDB_Min_But.
function VLDB_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBMin;

% --- Executes on button press in SIG_Orig_But.
function SIG_Orig_But_Callback(hObject, eventdata, handles)
% hObject    handle to SIG_Orig_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigScript;

% --- Executes on button press in Sig_Min_But.
function Sig_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sig_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigMin;

% --- Executes on button press in KDD_Matrix_But.
function KDD_Matrix_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDImage;

% --- Executes on button press in Sigmod_Matrix_But.
function Sigmod_Matrix_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sigmod_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigmodImage;

% --- Executes on button press in VLDB_Matrix_But.
function VLDB_Matrix_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBImage;

% --- Executes on button press in ICDE_Matrix_But.
function ICDE_Matrix_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEImage;


% --- Executes on button press in KDD_Matrix_But.
function pushbutton29_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in KDD_Min_But.
function KDD_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ICDE_Matrix_But.
function pushbutton31_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Matrix_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ICDE_MIN_But.
function ICDE_MIN_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_MIN_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEMin;

% --- Executes on button press in VLDB_Gaprh_But.
function VLDB_Gaprh_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Gaprh_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBScript;

% --- Executes on button press in VLDB_MIN_But.
function VLDB_MIN_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_MIN_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBMin;

% --- Executes on button press in Sig_Graph_But.
function Sig_Graph_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sig_Graph_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigScript;

% --- Executes on button press in Sig_MIN_But.
function Sig_MIN_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sig_MIN_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigMin;