%Module To Compute Matrix Multiplication For Baseline Relation
%Property Of Ishaq Mariam Rashid

function [A] = Matrix_Base(M1,M2,M3,M4)

[A] = M1 .*0.25 + M2.*0.25+ M3.*0.25 + M4.*0.25;