%KDD Script
%Property Of Ishaq Mariam Rashid

%xlsread takes Input
KDD = xlsread('Conference_Graph.xlsx',-1);

%Computes the Upper Triangular Matrix
KDD = triu(KDD);

%Constructs a Sparse Matrix
KDD_O = sparse([1 1 1 1 1 2 2 2 2 3 3 3 4 4 5 ],[2 3 4 5 6 3 4 5 6 4 5 6 5 6 6],[ 0.4444 0.5556 0.7778 0.7778 0.7778 0.6667 0.6667 0.5556 0.6667 0.6667 0.5556 0.6667 0.8889 1 0.8889],6,6);

%Calls The Mimimum Cut Algorithm
%MaxFlow - Computes The Maximum Flow Betweewn Noodes
%Flow - Stores the Edge Values
%Cut - Stores the inimum Cut Value
[MaxFlow,Flow,Cut] = graphmaxflow(KDD_O,1,6)

%Shows The Constructed Graph
graph = view(biograph(KDD_O,[],'ShowWeights','on'));

%View The Graph With Flow Values
view(biograph(Flow,[],'ShowWeights','on'))

% Show The Minimum Cut Of The Solution
set(graph.Nodes(Cut(1,:)),'Color',[0 0 1])