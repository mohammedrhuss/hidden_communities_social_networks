 function  SpectralClusteringModule(Input,S,k)
% Property Of Ishaq Mariam Rashid
%(Graph): Let G = (V; E ) with a non-empty and finite set V ={V1,.,.,.,.,.Vn}
%of vertices and a set E  V X V of edges between such vertices.
%Then G is called a (directed) graph. If (Vi,Vj)belongs to E,we denote this edge by Eij.
%Moreover, if E is symmetric, that is E1j belongs to E and we call G an undirected graph.

%Note: Affinity Matrix already provided as input

%Step-1 Construct the degree Matrix   
% A degree Matrix for an undirected weighted graph is di=sum of weight of i and j(where j=1 to n)
% FORMALLY, D=diag{d1,.....dn} is called the degree matrix of Graph(i.e Input Matrix)

for i=1:size(S,1)
    D(i,i) = sum(S(i,:));
end

%Step-2 Construct the Laplasian Matrices
%These matrices are the one that hold much of the information about the graph
%It can be constructed in various forms 
%for unnormalized graph Laplacian is constructed as L=D-W (where W is the normal weighted graph(in our case W can be the weighted input matrix)
%Inorder to get normalized graph laplasian we can use 2 forms.In our algorithm we select the first form: Lsys=D^1/2*L*D^1/2

for i=1:size(S,1)
    for j=1:size(S,2)
        NL1(i,j) = S(i,j) / (sqrt(D(i,i)) * sqrt(D(j,j)));  
    end
end

%Step-3
% perform the eigen value decomposition
[eigVectors,eigValues] = eig(NL1);

%Step-4
% select k largest eigen vectors
%k3;
nEigVec = eigVectors(:,(size(eigVectors,1)-(k-1)): size(eigVectors,1));

%Step-5
% construct the normalized matrix U from the obtained eigen vectors
for i=1:size(nEigVec,1)
    n = sqrt(sum(nEigVec(i,:).^2));    
    U(i,:) = nEigVec(i,:) ./ n; 
end

%Step-6
% perform kmeans clustering on the matrix U
[IDX,C] = kmeans(U,k); 

%Step-7
% plot the eigen vector corresponding to the largest eigen value
%figure,plot(IDX)
figure,
hold on;
for i=1:size(IDX,1)
    if IDX(i,1) == 1
        plot(Input(i,1),U(i,2),'ro');
    elseif IDX(i,1) == 2
        plot(Input(i,1),U(i,2),'bo');
    else
        plot(Input(i,1),U(i,2),'mo');        
    end
end
hold off;
title('Clustering Results using K-means');
grid on;


