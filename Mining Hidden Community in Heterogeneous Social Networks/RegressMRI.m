%Automated Regression Module
%Property of IShaq Mariam Rashid

% To Take Input
Input = xlsread('Iris.xlsx',-1);

%Taking Individual Matrices
disp('Sepal Length Values');
SL = Input(:,1)
disp('Sepal Width Values');
SW = Input(:,2)
disp('Petal Length Values');
PL = Input(:,3)
disp('Petal Width Values');
PW = Input(:,4)
disp('Class Values');
Y  = Input(:,5)

%Take No Of Rows

evalR = input('Enter Number Of Rows Chosen \n')

%Data Preparation For Each Feature Linear Regression
disp('Data For SL LR');
F1 = SingleM(SL,evalR)
disp('Data For SW LR');
F2 = SingleM(SW,evalR)
disp('Data For PL LR');
F3 = SingleM(PL,evalR)
disp('Data For PW LR');
F4 = SingleM(PW,evalR)

%Apply Linear Regression To Obtain Coefficients
disp('Coeffient For Feature 1');
B1 = RegressionModule(F1,Y)
disp('Coeffient For Feature 2');
B2 = RegressionModule(F2,Y)
disp('Coeffient For Feature 3');
B3 = RegressionModule(F3,Y)
disp('Coeffient For Feature 4');
B4 = RegressionModule(F4,Y)

%Check For Constraint Satisfaction
Constraints(B1,B2,B3,B4)

%Constructing Similarity Matrix
disp('Similarity Matrix For Feature SL');
M1 = Similari(Input,1,evalR)
disp('Similarity Matrix For Feature SW');
M2 = Similari(Input,2,evalR)
disp('Similarity Matrix For Feature PL');
M3 = Similari(Input,3,evalR)
disp('Similarity Matrix For Feature PW');
M4 = Similari(Input,4,evalR)

%Show Images of Matrices
%disp('Images For Feature SL');
%imagesc(M1)
%disp('Images For Feature SW');
%imagesc(M2)
%disp('Images For Feature PL');
%imagesc(M3)
%disp('Images For Feature PW');
%imagesc(M4)

%Construct Final Matrix
disp('Final Matrix');
M = Matrix_Mul(M1,M2,M3,M4,B1,B2,B3,B4)

% Take Number Of Cluster Value
K = input('Enter Number Of Clusters \n')

%Spectral Clustering With Final Matrix
SpecClus(Input,M,K)

