%ICDE Script
%Property Of Ishaq Mariam Rashid

%xlsread takes Input
ICDE = xlsread('Conference_Graph.xlsx',-1);

%Computes the Upper Triangular Matrix
ICDE = triu(ICDE);

%Constructs a Sparse Matrix
ICDE_O = sparse([1 1 1 2 2 3 5],[2 3 5 5 6 4 6],[0.6667 0.3333 0.3333 0.6667 0.6667 0.3333 0.3333],6,6);

%Calls The Mimimum Cut Algorithm
%MaxFlow - Computes The Maximum Flow Betweewn Noodes
%Flow - Stores the Edge Values
%Cut - Stores the inimum Cut Value
[MaxFlow,Flow,Cut] = graphmaxflow(ICDE_O,1,6)

%Shows The Constructed Graph
graph = view(biograph(ICDE_O,[],'ShowWeights','on'));

%View The Graph With Flow Values
view(biograph(Flow,[],'ShowWeights','on'))

% Show The Minimum Cut Of The Solution
% set(graph.Nodes(Cut(:,:)),'Color',[0 0 1])