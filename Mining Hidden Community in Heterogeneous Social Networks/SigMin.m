%Sigmod Script
%Property Of Ishaq Mariam Rashid

%xlsread takes Input
Sig = xlsread('Conference_Graph.xlsx',-1);

%Computes the Upper Triangular Matrix
Sig = triu(Sig);

%Constructs a Sparse Matrix
S = sparse([1 1 2 2 3 4 4 5],[2 5 3 6 4 5 6 6],[1 0.333 1 0.333 0.6667 0.3333 0.333 0.6667],6,6)

%Calls The Minimum Cut Algorithm
%MaxFlow - Computes The Maximum Flow Betweewn Noodes
%Flow - Stores the Edge Values
%Cut - Stores the minimum Cut Value
[MaxFlow,Flow,Cut] = graphmaxflow(S,1,6)

%Shows The Constructed Graph
graph = view(biograph(S,[],'ShowWeights','on'));

%View The Graph With Flow Values
view(biograph(Flow,[],'ShowWeights','on'))

% Show The Minimum Cut Of The Solution
set(graph.Nodes(Cut(1,:)),'Color',[0 0 1])