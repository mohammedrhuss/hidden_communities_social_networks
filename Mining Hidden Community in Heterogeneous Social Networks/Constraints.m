%Module To Check if Constraint Satisfied
%Property Of Ishaq Mariam Rashid
function Constraints(A,B,C,D)
   
K = A(2)^2 + B(2)^2 + C(2)^2 + D(2)^2;
if K <=1
        disp('Constraint Satisfied');
end