%Regression Module
%Property of Ishaq Mariam Rashid
%This Module is responsible for computing the Linear Regression involved
%the algorithm.

function [Coef] = RegressionModule(V,Y)
    % V Here is Single Matrix
    
    %Coeff = (V'V)^-1*V'*Y
    
    %Finding Transpose Of Matrix V
    VTrans = transpose(V);

    %Calculating Intermediate Value
    VCal = VTrans * V;
    
    %Inverse Matrix
    Vin = inv(VCal);

    %Identity Matrix
    %I = eye(5);
    
    %Computing RHS
    %A = VTrans * Y;
    
    %Computing Coefficient
    %Co = I\VCal;
    
    %Cpefficeint
    Coef = Vin * VTrans *Y;
    
    %Coef = Cu(2);

