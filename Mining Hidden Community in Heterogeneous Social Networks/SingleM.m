%Module To Make For Linear Regressions
%Property Of Ishq Mariam Rashid
% This Module is responsible for preparing data for linear Regression
% The data/output obtained here is utilised to give as input for Regression
%Module.m
function [X] =  SingleM(A,n)

% Here A Is Feature Column

X = [ones(n,1),A]; % Returns Matrix For Linear Regression  