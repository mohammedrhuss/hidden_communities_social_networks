%Module To Compute Matrix Multiplication With Respective Coeffcient
%Property Of Ishaq Mariam Rashid

function [A] = Matrix_Mul(M1,M2,M3,M4,B1,B2,B3,B4)

[A] = M1*B1(2) + M2*B2(2) + M3*B3(2) + M4*B4(2);