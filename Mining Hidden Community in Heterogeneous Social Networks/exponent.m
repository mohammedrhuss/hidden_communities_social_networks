%Module to compute Exponent Matrix
%Property Of Ishaq Mariam Rashid
%This module is utilized in forming the Affinity Matrix Used
%in Spectral Clustering.
function  [C] = exponent(X,Y)

% X is first value of same label
% Y is second value of same label

A = ((X-Y).^2);
C = exp(-A);