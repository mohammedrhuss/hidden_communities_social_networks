 function  SpecClus(Input,S,k)
% Property Of Ishaq Mariam Rashid

%If Edge E is symmetric, that is Eij belongs to E and we call G an undirected graph.

%Affinity Matrix already provided as input

%Step-1 Construct the degree Matrix   
%A degree Matrix for an undirected weighted graph is di=sum of weight of i and j(where j=1 to n)
%D=diag{d1,.....dn} is called the degree matrix of Graph(i.e Input Matrix)

for i=1:size(S,1)
    D(i,i) = sum(S(i,:));		%This code computes for the entire column
end

%Step-2 Construct the Laplasian Matrices
%These matrices are the one that hold much of the information about the graph
%It can be constructed in various forms 
%for unnormalized graph Laplacian is constructed as L=D-W (where W is the normal weighted graph(in our case W can be the weighted input matrix)
%Inorder to get Normalized Graph Laplasian(NGL) we can use 2 forms.In our algorithm we select the first form: Lsys=D^1/2*L*D^1/2

for i=1:size(S,1)
    for j=1:size(S,2)
        NGL1(i,j) = S(i,j) / (sqrt(D(i,i)) * sqrt(D(j,j)));  
    end
end

%Step-3
% Perform the eigen value decomposition
[eigVectors,eigValues] = eig(NGL1);

%Step-4
% select k largest eigen vectors
nEigVec = eigVectors(:,(size(eigVectors,1)-(k-1)): size(eigVectors,1));

%Step-5
% construct the normalized matrix U from the obtained eigen vectors
for i=1:size(nEigVec,1)
    n = sqrt(sum(nEigVec(i,:).^2));    
    U(i,:) = nEigVec(i,:) ./ n; 
end

%Step-6
% perform kmeans clustering on the matrix U
[IDX,C] = kmeans(U,k); 

%Step-7
% plot the eigen vector corresponding to the largest eigen value
%figure,plot(IDX)
figure,
hold on;
for i=1:size(IDX,1)
    if IDX(i,1) == 1
        plot(Input(i,1),U(i,2),'ro');
    elseif IDX(i,1) == 2
        plot(Input(i,1),U(i,2),'bo');
    else
        plot(Input(i,1),U(i,2),'mo');        
    end
end
hold off;
title('Spectal Clustring');
grid on;


