%Similarity Matrix Module
%Property Of Ishaq Mariam Rashid
%Special Thanks To Adnan Rashid

function [A] = Similari(F,i,n)

% n specifies the dimensions of the Matrix
% F is the matrix that contains the data


%for i = 1:n
   [A] =  ProcessColumn(F,i,n);
%end
    

function  [Z] = ProcessColumn(F,i,n)

%fprintf('Printing Result Of Feature Class : %d  \n',i);
%n = 5;
for r = 1:n
    [source] = F(r,i);
    [sourceClass] = F(r,n);
    
    for c = 1:n
        if r == c
              output(r,c) = 1;
        else 
            targetClass = F(c,n);
                if sourceClass == targetClass
                    XiMinusXj = (source - F(c,i));
                    XiMinusXj = XiMinusXj^2;
                    XiMinusXj = -1 * XiMinusXj;
                    exponent = exp(XiMinusXj);
                    output(r,c) = exponent;
                else
                    output(r,c) = 0;
                end
        end
    end
end

%Display The Output


Z = output;