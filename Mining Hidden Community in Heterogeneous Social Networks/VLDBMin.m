%VLDB Script
%Property Of Ishaq Mariam Rashid

%xlsread takes Input
VLDB = xlsread('Conference_Graph.xlsx',-1);

%Computes the Upper Triangular Matrix
VLDB = triu(VLDB);

%Constructs a Sparse Matrix
VLDB_O = sparse([1 1 1 2 2 3 ],[2 3 6 3 6 6 ],[0.8 1 0.4 0.8 0.2 0.2],6,6);

%Calls The Mimimum Cut Algorithm
%MaxFlow - Computes The Maximum Flow Betweewn Noodes
%Flow - Stores the Edge Values
%Cut - Stores the inimum Cut Value
[MaxFlow,Flow,Cut] = graphmaxflow(VLDB_O,1,6)

%Shows The Constructed Graph
graph = view(biograph(VLDB_O,[],'ShowWeights','on'));

%View The Graph With Flow Values
view(biograph(Flow,[],'ShowWeights','on'))

% Show The Minimum Cut Of The Solution
set(graph.Nodes(Cut(1,:)),'Color',[0 0 1])