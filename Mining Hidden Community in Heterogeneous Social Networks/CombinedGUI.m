function varargout = CombinedGUI(varargin)
% COMBINEDGUI MATLAB code for CombinedGUI.fig
%      COMBINEDGUI, by itself, creates a new COMBINEDGUI or raises the existing
%      singleton*.
%
%      H = COMBINEDGUI returns the handle to a new COMBINEDGUI or the handle to
%      the existing singleton*.
%
%      COMBINEDGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMBINEDGUI.M with the given input arguments.
%
%      COMBINEDGUI('Property','Value',...) creates a new COMBINEDGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CombinedGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CombinedGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CombinedGUI

% Last Modified by GUIDE v2.5 20-Mar-2013 11:17:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CombinedGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CombinedGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CombinedGUI is made visible.
function CombinedGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CombinedGUI (see VARARGIN)

% Choose default command line output for CombinedGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CombinedGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CombinedGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Regression_Button.
function Regression_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Regression_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MRIGui;

% --- Executes on button press in Min_But.
function Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MRIMin;
