function varargout = MRIGui(varargin)
% MRIGUI MATLAB code for MRIGui.fig
%      MRIGUI, by itself, creates a new MRIGUI or raises the existing
%      singleton*.
%
%      H = MRIGUI returns the handle to a new MRIGUI or the handle to
%      the existing singleton*.
%
%      MRIGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MRIGUI.M with the given input arguments.
%
%      MRIGUI('Property','Value',...) creates a new MRIGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MRIGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MRIGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MRIGui

% Last Modified by GUIDE v2.5 14-Mar-2013 14:31:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MRIGui_OpeningFcn, ...
                   'gui_OutputFcn',  @MRIGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MRIGui is made visible.
function MRIGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MRIGui (see VARARGIN)

% Choose default command line output for MRIGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MRIGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MRIGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Click_but.
function Click_but_Callback(hObject, eventdata, handles)
% hObject    handle to Click_but (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
RegressMRI;

% --- Executes on button press in SL_But.
function SL_But_Callback(hObject, eventdata, handles)
% hObject    handle to SL_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SL_But;

% --- Executes on button press in PW_But.
function PW_But_Callback(hObject, eventdata, handles)
% hObject    handle to PW_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PW_But;

% --- Executes on button press in PL_Butt.
function PL_Butt_Callback(hObject, eventdata, handles)
% hObject    handle to PL_Butt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PL_But;

% --- Executes on button press in SW_But.
function SW_But_Callback(hObject, eventdata, handles)
% hObject    handle to SW_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SW_But;
