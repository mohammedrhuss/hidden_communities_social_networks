function varargout = MRIMin(varargin)
% MRIMIN MATLAB code for MRIMin.fig
%      MRIMIN, by itself, creates a new MRIMIN or raises the existing
%      singleton*.
%
%      H = MRIMIN returns the handle to a new MRIMIN or the handle to
%      the existing singleton*.
%
%      MRIMIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MRIMIN.M with the given input arguments.
%
%      MRIMIN('Property','Value',...) creates a new MRIMIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MRIMin_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MRIMin_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MRIMin

% Last Modified by GUIDE v2.5 20-Mar-2013 20:14:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MRIMin_OpeningFcn, ...
                   'gui_OutputFcn',  @MRIMin_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MRIMin is made visible.
function MRIMin_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MRIMin (see VARARGIN)

% Choose default command line output for MRIMin
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MRIMin wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MRIMin_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in KDD_Graph_But.
function KDD_Graph_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Graph_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDScript;

% --- Executes on button press in ICDE_Graph_But.
function ICDE_Graph_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Graph_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEScript;

% --- Executes on button press in VLDB_Graph_But.
function VLDB_Graph_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Graph_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBScript;

% --- Executes on button press in Sigmod_Graph_But.
function Sigmod_Graph_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sigmod_Graph_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigScript;

% --- Executes on button press in KDD_Min_But.
function KDD_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDMin;

% --- Executes on button press in ICDE_Min_But.
function ICDE_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEMin;

% --- Executes on button press in VLDB_Min_But.
function VLDB_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBMin;

% --- Executes on button press in Sigmod_Min_But.
function Sigmod_Min_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sigmod_Min_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigMin;

% --- Executes on button press in KDD_Mat_But.
function KDD_Mat_But_Callback(hObject, eventdata, handles)
% hObject    handle to KDD_Mat_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
KDDImage;

% --- Executes on button press in ICDE_Mat_But.
function ICDE_Mat_But_Callback(hObject, eventdata, handles)
% hObject    handle to ICDE_Mat_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ICDEImage;

% --- Executes on button press in VLDB_Mat_But.
function VLDB_Mat_But_Callback(hObject, eventdata, handles)
% hObject    handle to VLDB_Mat_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
VLDBImage;

% --- Executes on button press in Sig_Mat_But.
function Sig_Mat_But_Callback(hObject, eventdata, handles)
% hObject    handle to Sig_Mat_But (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SigmodImage;
